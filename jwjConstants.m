#import "jwjConstants.h"

NSString * const PATHIDJWJ=@"path";
NSString * const USERIDJWJ=@"user";

NSString * const SIZEIDJWJ=@"maxsize";
NSString * const MTIMEIDJWJ=@"mtime";
NSString * const EXCLUDEIDJWJ=@"exclude";
NSString * const INCLUDEIDJWJ=@"include";
NSString * const SEPIDJWJ=@",";

NSString * const FILTERFILEIDJWJ=@"favboxFilter.plist";
NSString * const GROUPFILEIDJWJ=@"favboxGroup.plist";