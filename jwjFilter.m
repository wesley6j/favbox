#import "jwjFilter.h"


@implementation jwjFilter

@synthesize filterItems=_filterItems;


-(void)loadFromList:(NSArray *)filters{
    _filterItems=[[NSMutableArray alloc] init];
    if (!filters || ![filters isKindOfClass:[NSArray class]] || [filters count]==0) {
        NSLog(@"FavBox-jwjFilter-loadFromList: invalid/empty filters, an empty filter is created:%@",filters.class);
        return;
    }
    for (id item in filters) {
        if ([item isKindOfClass:[NSDictionary class]]) {
            [_filterItems addObject:[item mutableCopy]];
        }
    }    
}
- (id)initWithURL:(NSURL *)url{
    self=[super init];
    if (!self) {
        return nil;
    }
    [self loadFromList:[NSArray arrayWithContentsOfURL:url]];
    return self;
}
- (BOOL)saveToURL:(NSURL *)url{
    if (self.filterItems) {
        if([self.filterItems writeToURL:url atomically:YES]){
            //NSLog(@"FavBox-filters saved");
            return YES;
        }
    }
    NSLog(@"Favbox-fail to save filters");
    return NO;
}


- (BOOL)path:(NSString *)path isSameAs:(NSString *)another{
    if ([path.capitalizedString isEqualToString:another.capitalizedString]) {
        return YES;
    }
    return NO;
    
}
- (BOOL)path:(NSString *)path isInFolder:(NSString *)folder{
    NSRange range=[path rangeOfString:folder options: NSCaseInsensitiveSearch];
    if (range.length==0) {
        return NO;
    }
    if (range.location!=0) {
        return NO;
    }
    return YES;
}
-(BOOL)path:(NSString *)path isOfExtension:(NSString *)ext{
    NSRange range=[path rangeOfString:ext options:NSBackwardsSearch | NSCaseInsensitiveSearch];
    if (range.length==0) {
        return NO;
    }
    if (range.location+range.length!=path.length) {
        return NO;
    }
    return YES;
}
- (NSMutableDictionary *)findFilterForFolder:(NSString *)path andUser:(NSString *)user{
    for (NSMutableDictionary *item in self.filterItems) {
        if ([self path:((NSString *)[item objectForKey:PATHIDJWJ]).capitalizedString isSameAs:path.capitalizedString] && [[item objectForKey:USERIDJWJ] isEqualToString:user]) {
            return item;
        }
    }
    return nil;
}
- (NSMutableDictionary *)findFilterForFolder:(NSString *)path andUser:(NSString *)user withAutoCreate:(BOOL)autocreate{
    NSMutableDictionary * item=[self findFilterForFolder:path andUser:user];
    if (!item && autocreate) {
        item=[[NSMutableDictionary alloc] init];
        [item setObject:path forKey:PATHIDJWJ];
        [item setObject:user forKey:USERIDJWJ];
        [self.filterItems addObject:item];
    }
    return item;
}
- (NSMutableDictionary *)closestFilterForPath:(NSString *)path andUser:(NSString *)user{
    NSMutableDictionary * result=nil;
    for (id item in self.filterItems) {
        if ([user isEqualToString:[item objectForKey:USERIDJWJ]] && [self path:path isInFolder:[item objectForKey:PATHIDJWJ]]) {
            if (result==nil) {
                result=item;
                continue;
            }
            if ([[result objectForKey:PATHIDJWJ] length] < [[item objectForKey:PATHIDJWJ] length]) {
                result=item;
            }
        }
    }
    return result;
}
- (void)setValue:(id)value forKey:(id)key atPath:(NSString *)path andUser:(NSString *)user{
    NSMutableDictionary * filter=[self findFilterForFolder:path andUser:user withAutoCreate:YES];
    [filter setObject:value forKey:key];
}
-(void)removeKey:(id)key atPath:(NSString *)path andUser:(NSString *)user{
    NSMutableDictionary * filter=[self findFilterForFolder:path andUser:user];
    if (!filter) {
        return;
    }
    [filter removeObjectForKey:key];
}
-(void)removePath:(NSString *)path forUser:(NSString *)user{
    NSMutableDictionary * filter=[self findFilterForFolder:path andUser:user];
    if(!filter){
        return;
    }
    [self.filterItems removeObject:filter];
}

-(BOOL)matchFileDate:(NSDate *)mdate size:(NSNumber *)size forPath:(NSString *)path andUser:(NSString *)user{
    NSMutableDictionary * filter=[self closestFilterForPath:path andUser:user];
    if (!filter) {
        //NSLog(@"match fail: no such folder[%@] for user[%@]", path,user);
        return NO;
    }
    NSNumber * tempNum;
    NSString * tempStr;
    tempNum=[filter objectForKey:MTIMEIDJWJ];
    if (tempNum!=nil) {
        float mtime=tempNum.floatValue;
        //NSLog(@"compare time: %f, %lf",mtime*3600*24, -[mdate timeIntervalSinceNow]);
        if (mtime>0 && mtime*3600*24 < -[mdate timeIntervalSinceNow]) {
            NSLog(@"match fail: time too old for path:%@",path);
            return NO;
        }
    }
    tempNum=[filter objectForKey:SIZEIDJWJ];
    if (tempNum!=nil) {
        float maxsize=tempNum.floatValue;
        //NSLog(@"compare size:%f,%lld",maxsize*1024*1024, size.longLongValue);
        if (maxsize>0 && maxsize*1024*1024 < size.longLongValue) {
            NSLog(@"match fail: size too large for path:%@",path);
            return NO;
        }
    }
    tempStr=[filter objectForKey:EXCLUDEIDJWJ];
    BOOL isExclude=YES;
    if (tempStr==nil) {
        tempStr=[filter objectForKey:INCLUDEIDJWJ];
        isExclude=NO;
        if (tempStr==nil) {
            return YES;
        }
    }
    tempStr=[tempStr stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSArray * list=[tempStr componentsSeparatedByString:SEPIDJWJ];
    for (NSString * ext in list) {
        if ([self path:path isOfExtension:ext]) {
            return !isExclude;
        }
    }
    return isExclude;
    
}
-(BOOL)matchFileMeta:(DBMetadata *)meta forUser:(NSString *)user{
    if (meta.isDirectory) {
        NSLog(@"favBox:matchFileMeta: meta points to a folder!");
        return NO;
    }
    if (meta.isDeleted) {
        NSLog(@"favBox:matchFileMeta: meta points to a deleted file");
        return NO;
    }
    return [self matchFileDate:meta.lastModifiedDate size:[NSNumber numberWithLongLong:meta.totalBytes] forPath:meta.path andUser:user];
}

-(BOOL)matchDirectoryEntry:(DropboxDirectoryEntry *)entry{
    if(entry.isDirectory){
        NSLog(@"favbox:matchDirectoryEntry:entry points to a folder");
        return NO;
    }
    if (entry.isDeleted) {
        NSLog(@"favbox:matchDirectoryEntry:entry points to a deleted file");
        return NO;
    }
    return [self matchFileDate:entry.modifiedDate size:[NSNumber numberWithLongLong:entry.bytes] forPath:entry.filePath andUser:entry.userId];
}
- (void) favoriteFolder:(NSString *)folder forUser:(NSString *)user{
    [self findFilterForFolder:folder andUser:user withAutoCreate:YES];
}
- (BOOL) isFolder:(NSString *)folder favoritedForUser:(NSString *)user{
    if([self findFilterForFolder:folder andUser:user]!=nil){
        return YES;
    }
    return NO;
}

@end