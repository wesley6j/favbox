#import "jwjFavManager.h"


@implementation jwjFavManager

- (id) init{
    self=[super init];
    if (!self) {
        return nil;
    }
    NSArray * list=[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                          inDomains:NSUserDomainMask];
    if (list && list.count>=1) {
        self.baseURL=[list objectAtIndex:0];
        self.filterURL=[[NSURL URLWithString:FILTERFILEIDJWJ relativeToURL:self.baseURL] absoluteURL];
        self.groupURL=[[NSURL URLWithString:GROUPFILEIDJWJ relativeToURL:self.baseURL] absoluteURL];
    }
    self.Filters=[[jwjFilter alloc] initWithURL:self.filterURL];
    self.GroupedFiles=[[jwjFilesInGroup alloc] initWithURL:self.groupURL];
    self.fileManagers=[[NSMutableDictionary alloc] init];
    return self;
}

- (void) saveFilter{
    [self.Filters saveToURL:self.filterURL];
    //NSLog(@"save filter settings with success?:%i",result);
}

- (void) saveGroup{
    [self.GroupedFiles saveToURL:self.groupURL];
    //NSLog(@"save group settings with success?:%i",result);
}

- (void) addFilemanager:(FileOperationsManager *)fm forUser:(NSString *)user{
    [self.fileManagers setObject:fm forKey:user];
}
- (void) removeFileManagerForUser:(NSString *)user{
    [self.fileManagers removeObjectForKey:user];
}
- (void) removeFilemanager:(FileOperationsManager *)fm{
    for (NSString * user in self.fileManagers) {
        FileOperationsManager * obj=[self.fileManagers objectForKey:user];
        if (obj.hash == fm.hash) {
            [self removeFileManagerForUser:user];
            break;
        }
    }
}

- (BOOL) isFavoritedEntry:(DropboxDirectoryEntry *)entry{
    for (DropboxDirectoryEntry * item in self.favoriteList.directoryEntries) {
        if ([entry.filePath.capitalizedString isEqualToString:item.filePath.capitalizedString] && [entry.userId isEqualToString:entry.userId]) {
            return YES;
        }
    }
    return NO;
}
- (void) favFileWithEntry:(DropboxDirectoryEntry *)entry inGroup:(BOOL)ingroup{
    if (!self.favoriteList) {
        //NSLog(@"no instance of DBGlobalFavoriteFiles=> fail to add favorite file");
        return;
    }
    if ([self isFavoritedEntry:entry]) {
        //NSLog(@"already favorited");
        return;
    }
    //NSLog(@"will add file with path: %@", entry.filePath);
    [self.favoriteList addDirectoryEntry:entry];
    if (ingroup) {
        //NSLog(@"apply group effect");
        [self.GroupedFiles addPath:entry.filePath forUser:entry.userId];
    }
}
- (void) unfavFileWithEntry:(DropboxDirectoryEntry *)entry inGroup:(BOOL)ingroup{
    if (!self.favoriteList) {
        //NSLog(@"no instance of DBGlobalFavoriteFiles=> fail to remove favorite file");
        return;
    }
    if (![self isFavoritedEntry:entry]) {
        //NSLog(@"file can not be removed because it was not added:%@", entry.filePath);
        return;
    }
    if (!ingroup) {
        [self.favoriteList removeDirectoryEntry:entry shouldLog:NO];
        //NSLog(@"remove file without affecting group: %@", entry.filePath);
        return;
    }
    
    if (ingroup && [self.GroupedFiles existPath:entry.filePath forUser:entry.userId]) {
        //NSLog(@"will remove file: %@",entry.filePath);
        [self.favoriteList removeDirectoryEntry:entry shouldLog:NO];
        [self.GroupedFiles removePath:entry.filePath forUser:entry.userId];
    }
}

- (void) updateFavFolder:(NSString *)path forUser:(NSString *)user{
    //NSLog(@"updateFavFolder:%@ forUser:%@", path,user);
    if (![self.Filters findFilterForFolder:path andUser:user withAutoCreate:NO]) {
        //NSLog(@"there is no such combination in filters:[%@,%@",path,user);
        return;
    }
    FileOperationsManager * fm=[self.fileManagers objectForKey:user];
    if (!fm) {
        //NSLog(@"there is no file operation manager for user:%@",user);
        return;
    }
    DBMetadata * folder=[fm metadataForPath:path];
    if (!folder.isDirectory || folder.contents==nil || folder.contents.count==0) {
        NSLog(@"the directory is not a folder or it is empty");
        return;
    }
    for (DBMetadata * item in folder.contents) {
        BOOL result=[self.Filters matchFileMeta:item forUser:user];
        
        if (result) {
            DropboxDirectoryEntry * entry=[[NSClassFromString(@"DropboxDirectoryEntry") alloc] initWithMetadata:item withUserId:user];
            [self favFileWithEntry:entry inGroup:YES];
        }
        else{
            //NSLog(@"item [%@] rejected.", item.path);
        }
    }
    [self.GroupedFiles saveToURL:self.groupURL];
    
}
- (void) cleanFavFolder:(NSString *)path forUser:(NSString *)user{
    //NSLog(@"cleanFavFolder:%@ forUser:%@", path,user);
    for (DropboxDirectoryEntry * item in self.favoriteList.directoryEntries) {
        if (![self.Filters path:item.filePath isInFolder:path]) {
            continue;
        }
        BOOL result=[self.Filters matchDirectoryEntry:item];
        if (!result) {
            [self unfavFileWithEntry:item inGroup:YES];
        }
    }
    [self.GroupedFiles saveToURL:self.groupURL];
}

- (void) refreshFavList{
    NSLog(@"refresh List");
    for (NSMutableDictionary * item in self.Filters.filterItems) {
        [self updateFavFolder:[item objectForKey:PATHIDJWJ] forUser:[item objectForKey:USERIDJWJ]];
    }
}

- (void) favFolderWithEntry:(DropboxDirectoryEntry *)entry{
    [self.Filters favoriteFolder:entry.filePath forUser:entry.userId];
    [self updateFavFolder:entry.filePath forUser:entry.userId];
    [self.Filters saveToURL:self.filterURL];
    //NSLog(@"folder %@ favorited", entry.filePath);
}
- (void) unfavFolderWithEntry:(DropboxDirectoryEntry *)entry{
    [self.Filters removePath:entry.filePath forUser:entry.userId];
    [self cleanFavFolder:entry.filePath forUser:entry.userId];
    [self.Filters saveToURL:self.filterURL];
    //NSLog(@"folder %@ unfavorited", entry.filePath);
}
- (BOOL) isFavoritedFolder:(DropboxDirectoryEntry *)entry{
    return [self.Filters isFolder:entry.filePath favoritedForUser:entry.userId];
}
@end