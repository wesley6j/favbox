#import "jwjFavManager.h"

#import "jwjFilter.h"
#import "jwjConstants.h"
#import "jwjFilesInGroup.h"

#import "DBGlobalFavoriteFiles.h"
#import "FileOperationsManager.h"
#import "DBMetadata.h"
#import "DropboxDirectoryEntry.h"
#import "FilesTableViewCell.h"
#import "FileListController.h"
#import <UIKit/UIKit.h>
static jwjFavManager		 * jwjFavManagerInstance=nil;

@interface DBDropboxSwipeAppearanceIOS7 : NSObject{}
-(id)swipeUnfavoritedImage;
-(id)swipeFavoritedImage;
@end
static DBDropboxSwipeAppearanceIOS7 * swipeAppearance=nil;

/*
 * hook to target classes in order to get a reference to the instance
 *
 */
%hook DBGlobalFavoriteFiles
-(id)init{
	id ret=%orig;
	if(jwjFavManagerInstance){
		jwjFavManagerInstance.favoriteList=ret;
		//NSLog(@"captured DBGlobalFavoriteFiles v1");
	}
	else{
		//NSLog(@"jwjFavManager not initiated");
	}
	return ret;
}
-(id)initWithUserStateManager:(id)arg1 {
	id ret=%orig(arg1);
	if(jwjFavManagerInstance){
		jwjFavManagerInstance.favoriteList=ret;
		//NSLog(@"captured DBGlobalFavoriteFiles v2");
	}
	else{
		//NSLog(@"jwjFavManager not initiated");
	}	
	return ret;
}
-(void)dealloc{
	jwjFavManagerInstance.favoriteList=nil;
	//NSLog(@"removed DBGlobalFavoriteFiles");
	%orig;
}
%end

%hook FileOperationsManager
-(id)initWithUserId:(id)arg1 usingFileCache:(id)arg2 {
	id fm=%orig(arg1,arg2);
	if(jwjFavManagerInstance){
		[jwjFavManagerInstance addFilemanager:fm forUser:(NSString *)arg1];
		//NSLog(@"captured FileOperationsManager for user:%@", arg1);
	}
	else{
		//NSLog(@"jwjFavManager not initiated");
	}
	return fm;
}
-(void)dealloc{
	[jwjFavManagerInstance removeFilemanager:self];
	//NSLog(@"removed FileOperationsManager");
	%orig;
}
%end

/*
 * modify SPActionView when necessary
 * 
 */
@interface SPActionView : UIImageView{}
-(void)updateImage:(id)arg1 atIndex:(long long)arg2 ;
-(id)addActionButtonWithImage:(id)arg1 block:(/*^block*/id)arg2 ;
@end
@interface FilesTableViewCell (FolderFavStatus)
 - (void) updateFavStatus;
@end

%hook DBDropboxSwipeAppearanceIOS7
- (id) init{
	id ret=%orig;
	if(ret){
		swipeAppearance=ret;
		//NSLog(@"captured appearance: %@", swipeAppearance);
	}
	return ret;
}
%end

%hook UIView
- (void)addSubview:(UIView *)view{
	if ([view isKindOfClass:NSClassFromString(@"SPActionView")]){
		if (self.superview && [self.superview isKindOfClass:NSClassFromString(@"FilesTableViewCell")] &&  view.subviews.count==3 && swipeAppearance){
			DropboxDirectoryEntry * entry=((FilesTableViewCell *)self.superview).directoryEntry;
			void (^action)();
			action=^(){
				//NSLog(@"favorite touched");
				if ([jwjFavManagerInstance isFavoritedFolder:entry]){
					[jwjFavManagerInstance unfavFolderWithEntry:entry];
					[(SPActionView *)view updateImage:[swipeAppearance swipeUnfavoritedImage] atIndex:3];
				}
				else{
					[jwjFavManagerInstance favFolderWithEntry:entry];
					[(SPActionView *)view updateImage:[swipeAppearance swipeFavoritedImage] atIndex:3];
				}
				[(FilesTableViewCell *)self.superview updateFavStatus];
			};
			if([jwjFavManagerInstance isFavoritedFolder:entry]){
				[(SPActionView *)view addActionButtonWithImage:[swipeAppearance swipeFavoritedImage] block:action];
			}
			else{
				[(SPActionView *)view addActionButtonWithImage:[swipeAppearance swipeUnfavoritedImage] block:action];
			}


		}
		
	}
	%orig(view);
}
%end

/*
 * refresh list every time dropbox enter foreground / enter fav table view
 * 
 */
%hook DropboxMainAppDelegate
-(void)applicationDidBecomeActive:(id)arg1 {
	%orig(arg1);
    [jwjFavManagerInstance performSelector:@selector(refreshFavList) withObject:nil afterDelay:0.7];
}
%end
%hook FavoritesTableViewController
-(void)viewDidAppear:(BOOL)arg1{
	%orig(arg1);
	[jwjFavManagerInstance refreshFavList];	
}
%end
/*
 * get table cell related entry
 * 
 */

%hook FilesTableViewCell

%new
- (void) updateFavStatus{
	DropboxDirectoryEntry * entry= self.directoryEntry;
	if (!entry || !entry.isDirectory){
		return;
	}
	NSArray * list1=((UIView *)self).subviews;
	if (!list1 || list1.count<1) return;
	UIView * level1=[list1 objectAtIndex:1];
	NSArray * list2=level1.subviews;
	if  (!list2 || list2.count<2) return;
	UIView * target=[list2 objectAtIndex:2];
	BOOL marked=(target.subviews.count==1);
	if ([jwjFavManagerInstance isFavoritedFolder:entry] && !marked){
		UIImageView * favicon=[[UIImageView alloc] initWithImage:[swipeAppearance swipeFavoritedImage]];
		favicon.frame=CGRectMake(
			target.frame.size.width*0.15,
			target.frame.size.height*0.20,
			target.frame.size.width*0.7,
			target.frame.size.height*0.7);
		[target addSubview:favicon];
	}
	else if(![jwjFavManagerInstance isFavoritedFolder:entry] && marked){
		[[target.subviews objectAtIndex:0] removeFromSuperview];
	}
}

-(void)updateWithDirectoryEntry:(DropboxDirectoryEntry *)entry syncStatus:(int)arg2 animated:(BOOL)arg3 cachedThumbnail:(id)arg4 withUserState:(id)arg5 {
	//NSLog(@"update v1 with user:%@ path:%@ (isFolder %i)", entry.userId, entry.filePath, entry.isDirectory);
	%orig(entry,arg2,arg3,arg4,arg5);
	[self updateFavStatus];
	
}
%end

%ctor{
	jwjFavManagerInstance=[[jwjFavManager alloc] init];
	NSLog(@"FavBox loading");
}
