#import <Foundation/Foundation.h>
@class DBUserStateManager, NSMutableArray;

@interface DBGlobalFavoriteFiles : NSObject {

	DBUserStateManager* _userStateManager;
	NSMutableArray* _directoryEntries;

}
+(id)db_directoryEntriesFromPath:(id)arg1 ;
+(id)db_directoryEntriesFromDictionaryArray:(id)arg1 ;
+(void)db_saveDirectoryEntries:(id)arg1 toPath:(id)arg2 ;
+(id)db_dictionaryArrayFromDirectoryEntries:(id)arg1 ;
+(id)db_directoryEntriesFromDisk;
+(void)db_saveDirectoryEntries:(id)arg1 ;
-(void)removeFilePath:(id)arg1 withUserId:(id)arg2 ;
-(void)updateDirectoryEntryListWithDirectoryEntryList:(id)arg1 withUserId:(id)arg2 ;
-(unsigned long long)numberOfFavoritesInAllAccounts;
-(id)directoryEntries;
-(long long)filesNeedingUpdating;
-(void)fetchAllLatestLocalCopies;
-(unsigned long long)indexOfDirectoryEntryWithFilePath:(id)arg1 withUserId:(id)arg2 ;
-(void)replaceDirectoryEntryAtIndex:(unsigned long long)arg1 withDirectoryEntry:(id)arg2 ;
-(void)removeDirectoryEntry:(id)arg1 shouldLog:(BOOL)arg2 ;
-(BOOL)isFavoriteWithFilePath:(id)arg1 withUser:(id)arg2 ;
-(void)addDirectoryEntry:(id)arg1 ;
-(void)moveFavoriteAtIndex:(unsigned long long)arg1 toIndex:(unsigned long long)arg2 ;
-(BOOL)canSyncDirectoryEntry:(id)arg1 ;
-(BOOL)fetchLatestLocalCopy:(id)arg1 ;
-(void)saveEntriesToDisk;
-(id)initWithUserStateManager:(id)arg1 ;
-(void)db_updateFavoritesByComparisonWithFileCache;
-(BOOL)fileNeedsUpdating:(id)arg1 ;
-(void)addDirectoryEntry:(id)arg1 startSync:(BOOL)arg2 ;
-(id)db_filePathToDirectoryEntryMapForUserId:(id)arg1 ;
-(id)directoryEntriesForUser:(id)arg1 ;
-(void)cancelLatestLocalCopyFetch:(id)arg1 ;
-(void)removeAllEntriesExceptUserIds:(id)arg1 ;
-(void)loadFile:(id)arg1 ;
-(void)dealloc;
-(id)init;
@end

