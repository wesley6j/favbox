/*
@interface DropboxDirectoryEntry : NSObject {}
@property (nonatomic,retain) NSString * filePath;                            //@synthesize filePath=_filePath - In the implementation block

@end

@interface DBMetadata : NSObject{}
@property (nonatomic,readonly) NSString * path; 
@property (nonatomic,readonly) BOOL isDirectory; 
@property (nonatomic,readonly) NSArray * contents; 
@property (nonatomic,readonly) NSString * humanReadableSize; 
@property (nonatomic,readonly) NSString * root; 
@property (nonatomic,readonly) NSString * icon; 
@property (nonatomic,readonly) long long revision; 
@property (nonatomic,readonly) NSString * rev; 
@property (nonatomic,readonly) NSString * filename; 
@end


@class DBGlobalFavoriteFiles;

static DBGlobalFavoriteFiles * favoriteListSingleton=nil;

%hook DBMetadata
-(id)description{
	NSString * result= [NSString stringWithFormat:@"DBMetadata:{path:%@,rev:%@,filename:%@;\ncontents:%@}", self.path, self.rev, self.filename, self.contents];
	return result;
}
%end

%hook DBGlobalFavoriteFiles
-(id)init{
	id ret=%orig;
	favoriteListSingleton=ret;
	NSLog(@"capture favoriteListSingleton v1: %@", favoriteListSingleton);
	return ret;
}
-(id)initWithUserStateManager:(id)arg1 {
	id ret=%orig(arg1);
	favoriteListSingleton=ret;
	NSLog(@"capture favoriteListSingleton v2: %@", favoriteListSingleton);
	return ret;
}
-(void)dealloc{
	favoriteListSingleton=nil;
	NSLog(@"remove favoriteListSingleton");
	%orig;
}
%end


%hook DropboxDirectoryEntry

-(id)initWithMetadata:(id)arg1 withUserId:(id)arg2 {
	id result=%orig(arg1,arg2);
	NSLog(@"initWithMetadata:%@",arg1);
	return result;
}
-(id)initWithDirectory:(id)arg1 withUpdatedFilenameHighlights:(id)arg2{
	id result=%orig(arg1,arg2);
	NSLog(@"initWithDirectory:%@",arg1);
	return result;
}
-(id)initWithPhotoItem:(id)arg1 withUserId:(id)arg2{
	id result=%orig(arg1,arg2);
	NSLog(@"initWithPhotoItem:%@",arg1);
	return result;
}
-(id)initWithDictionary:(id)arg1{
	id result=%orig(arg1);
	NSLog(@"initWithDictionary:%@",arg1);
	return result;
}
-(void)dealloc{
	NSLog(@" the item will be deallocated: %@",self.filePath);
	%orig;
}
%end


%ctor{
	NSLog(@"boxkeeper starting");
}

*/

@interface DBRestClient : NSObject {}
@property (assign,nonatomic) id delegate; 
@end

%hook DBRestClient

-(id)filesURLForPath:(id)arg1 {
	id b=%orig(arg1);
	NSLog(@"filesURLForPath: %@ => %@  <delegate>:%@", arg1, b,self.delegate);
	return b;
}
-(id)requestWithHost:(id)arg1 path:(id)arg2 parameters:(id)arg3 {
	id b=%orig(arg1, arg2, arg3);
	NSLog(@"requestWithHost: %@ path: %@ parameters: %@=> %@   <delegate>:%@", arg1,arg2, arg3, b,self.delegate);
	return b;	
}
-(id)requestWithHost:(id)arg1 path:(id)arg2 parameters:(id)arg3 method:(id)arg4 {
	id b=%orig(arg1, arg2, arg3,arg4);
	NSLog(@"requestWithHost: %@ path: %@ parameters: %@ method: %@ => %@ <delegate>:%@", arg1,arg2, arg3, arg4, b,self.delegate);
	return b;
}
-(id)requestWithHost:(id)arg1 version:(id)arg2 path:(id)arg3 parameters:(id)arg4 method:(id)arg5 {
	id b=%orig(arg1, arg2, arg3,arg4, arg5);
	NSLog(@"requestWithHost: %@ version:%@ path: %@ parameters: %@ method: %@ => %@ <delegate>:%@", arg1,arg2, arg3, arg4, arg5, b, self.delegate);
	return b;
}


-(void)modifiedLoadMetadata:(id)arg1 withParams:(id)arg2 {
	NSLog(@"modifiedLoadMetadata:%@ withParams: %@ <delegate>:%@", arg1, arg2,self.delegate);
	%orig(arg1,arg2);
}

-(void)requestDidLoadMetadataList:(id)arg1 {
	NSLog(@"requestDidLoadMetadataList:%@ <delegate>:%@",arg1,self.delegate);
	%orig(arg1);
}
-(void)requestDidLoadMetadata:(id)arg1 {
	NSLog(@"requestDidLoadMetadata:%@ <delegate>:%@",arg1,self.delegate);
	%orig(arg1);
}


-(void)loadMetadata:(id)arg1 {
	NSLog(@"loadMetadata:%@ <delegate>:%@",arg1,self.delegate);
	%orig(arg1);
}
-(void)loadMetadata:(id)arg1 withHash:(id)arg2 {
	NSLog(@"loadMetadata:%@ withHash: %@ <delegate>:%@",arg1,arg2,self.delegate);
	%orig(arg1,arg2);
}
-(void)loadMetadata:(id)arg1 withParams:(id)arg2 {
	NSLog(@"loadMetadata:%@ withParams: %@ <delegate>:%@",arg1,arg2,self.delegate);
	%orig(arg1,arg2);
}

-(void)parseMetadataWithRequest:(id)arg1 resultThread:(id)arg2 {
	NSLog(@"parseMetadataWithRequest:%@ resultThread: %@ <delegate>:%@",arg1,arg2,self.delegate);
	%orig(arg1,arg2);
}
-(void)didParseMetadata:(id)arg1 {
	NSLog(@"didParseMetadata:%@ <delegate>:%@",arg1,self.delegate);
	%orig(arg1);
}
-(void)parseMetadataFailedForRequest:(id)arg1 {
	NSLog(@"parseMetadataFailedForRequest:%@ <delegate>:%@",arg1,self.delegate);
	%orig(arg1);
}
-(void)loadMetadata:(id)arg1 atRev:(id)arg2 {
	NSLog(@"loadMetadata:%@ atRev: %@ <delegate>:%@",arg1,arg2,self.delegate);
	%orig(arg1,arg2);
}

%end






