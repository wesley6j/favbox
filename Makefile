TARGET = iphone:8.1:6.1
ARCHS = armv7 armv7s arm64
include $(THEOS)/makefiles/common.mk

TWEAK_NAME = FavBox
FavBox_FILES = jwjConstants.m jwjFilter.m jwjFilesInGroup.m jwjFavManager.m Tweak.xm
FavBox_FRAMEWORKS = UIKit
include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "killall -9 Dropbox"
