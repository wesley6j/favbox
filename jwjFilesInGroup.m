//
//  jwjFilesInGroup.m
//  FavBox
//
//  Created by Weijie JIN on 16/11/2014.
//  Copyright (c) 2014 Wesley6j. All rights reserved.
//

#import "jwjFilesInGroup.h"

@implementation jwjFilesInGroup
@synthesize listOfItems=_listOfItems;
- (void) addListOfItems:(NSDictionary *)listOfItems{
    _listOfItems=[[NSMutableDictionary alloc] init];
    if (!listOfItems || ![listOfItems isKindOfClass:[NSDictionary class]]) {
        NSLog(@"invalid/empty files in groups:%@",listOfItems.class);
        return;
    }
    for (NSString * user in listOfItems) {
        [_listOfItems setObject:[[listOfItems objectForKey:user] mutableCopy] forKey:user];
    }
}
- (id) initWithURL:(NSURL *)url{
    self=[super init];
    if (!self) {
        return nil;
    }
    [self addListOfItems:[NSDictionary dictionaryWithContentsOfURL:url]];
    return self;
}
- (BOOL) saveToURL:(NSURL *)url{
    BOOL success=[self.listOfItems writeToURL:url atomically:YES];
    //NSLog(@"save files group is success?:%i",success);
    return success;
}
- (void) addPath:(NSString *)path forUser:(NSString *)user{
    if ([self existPath:path forUser:user]) {
        return;
    }
    NSMutableArray * list=[self.listOfItems objectForKey:user];
    if (list==nil) {
        list=[[NSMutableArray alloc] initWithObjects:path, nil];
        [self.listOfItems setObject:list forKey:user];
    }
    [list addObject:path];
}
- (void) removePath:(NSString *)path forUser:(NSString *)user{
    if (![self existPath:path forUser:user]) {
        return;
    }
    NSMutableArray * list=[self.listOfItems objectForKey:user];
    if (list==nil) {
        return;
    }
    [list removeObject:path];
}
- (BOOL) existPath:(NSString *)path forUser:(NSString *)user{
    NSMutableArray * list=[self.listOfItems objectForKey:user];
    if (list==nil) {
        return NO;
    }
    if ([list containsObject:path]) {
        return YES;
    }
    return NO;
}
@end
