
#import "DBGlobalFavoriteFiles.h"
#import "FileOperationsManager.h"
#import "DBMetadata.h"
#import "DropboxDirectoryEntry.h"
#import "jwjFilter.h"
#import "jwjFilesInGroup.h"
#import "jwjConstants.h"


@interface jwjFavManager : NSObject
@property(strong, nonatomic) NSMutableDictionary * fileManagers;
@property(strong, nonatomic) DBGlobalFavoriteFiles * favoriteList;
@property(strong, nonatomic) jwjFilter * Filters;
@property(strong, nonatomic) jwjFilesInGroup * GroupedFiles;
@property(strong, nonatomic) NSURL * baseURL;
@property(strong, nonatomic) NSURL * filterURL;
@property(strong, nonatomic) NSURL * groupURL;
- (id) init;
- (void) addFilemanager:(FileOperationsManager *)fm forUser:(NSString *)user;
- (void) removeFilemanager:(FileOperationsManager *)fm;

- (void) favFileWithEntry:(DropboxDirectoryEntry *)entry inGroup:(BOOL)ingroup;
- (void) unfavFileWithEntry:(DropboxDirectoryEntry *)entry inGroup:(BOOL)ingroup;

- (void) favFolderWithEntry:(DropboxDirectoryEntry *)entry;
- (void) unfavFolderWithEntry:(DropboxDirectoryEntry *)entry;
- (BOOL) isFavoritedFolder:(DropboxDirectoryEntry *)entry;

- (void) updateFavFolder:(NSString *)path forUser:(NSString *)user;
- (void) cleanFavFolder:(NSString *)path forUser:(NSString *)user;

- (void) refreshFavList;


- (void) saveFilter;
- (void) saveGroup;
@end
