
@interface DropboxDirectoryEntry : NSObject {}

@property (nonatomic,readonly) BOOL isDirectory;
@property (nonatomic,readonly) BOOL isDeleted;  
@property (nonatomic,readonly) unsigned long long bytes; 
@property (nonatomic,readonly) NSDate * modifiedDate; 
@property (nonatomic,retain) NSString * filePath;   
@property (nonatomic,readonly) NSString * userId;   
@property (nonatomic,readonly) NSString * filename; 
@property (readonly) unsigned long long hash; 
-(id)initWithMetadata:(id)arg1 withUserId:(id)arg2 ;
-(id)clientModifiedDate;
-(BOOL)isSameFileAs:(id)arg1 ;
-(id)folderListFilename;
-(id)folderListDate;
-(id)initWithDirectory:(id)arg1 withUpdatedFilenameHighlights:(id)arg2 ;
-(id)contentsAsDictionary;
-(BOOL)isInPublicFolder;
-(void)dealloc;
-(BOOL)isEqual:(id)arg1 ;
-(unsigned long long)hash;
-(NSString *)description;
-(id)initWithDictionary:(id)arg1 ;
-(unsigned long long)bytes;
-(void)setFilePath:(NSString *)arg1 ;
-(NSString *)filePath;
-(BOOL)isPackage;
-(BOOL)isDirectory;
-(NSString *)userId;
-(unsigned long long)revision;
-(NSDate *)modifiedDate;
-(NSString *)filename;
-(BOOL)isReadOnly;
-(BOOL)isDeleted;
-(NSString *)iconName;
-(NSString *)rev;
@end

