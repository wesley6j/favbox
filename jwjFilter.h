#import <Foundation/Foundation.h>

#import "DBMetadata.h"
#import "DropboxDirectoryEntry.h"
#import "jwjConstants.h"

/*
 [
 {user:value,
  directory:value,
  size:val,
  date:val,
 include(exclude):vallist
 },
 {}
 ]
 */

@interface jwjFilter : NSObject{

}
@property(nonatomic,strong)NSMutableArray * filterItems;

- (id)initWithURL:(NSURL *)url;
- (void)loadFromList:(NSArray *)filters;
- (BOOL)saveToURL:(NSURL *)url;

- (BOOL)path:(NSString *)path isInFolder:(NSString *)folder;

-(void)setValue:(id)value forKey:(id)key atPath:(NSString *) path andUser:(NSString *)user;
-(void)removeKey:(id)key atPath:(NSString *)path andUser:(NSString *)user;
-(void)removePath:(NSString *)path forUser:(NSString *)user;

- (NSMutableDictionary *)findFilterForFolder:(NSString *)path andUser:(NSString *)user withAutoCreate:(BOOL)autocreate;
-(BOOL)matchFileMeta:(DBMetadata *)meta forUser:(NSString *)user;
-(BOOL)matchDirectoryEntry:(DropboxDirectoryEntry *)entry;

- (void) favoriteFolder:(NSString *)folder forUser:(NSString *)user;
- (BOOL) isFolder:(NSString *)folder favoritedForUser:(NSString *)user;

@end