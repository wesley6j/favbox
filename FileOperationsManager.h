
@protocol OS_dispatch_queue;
@class NSString, DBFileCache, NSMutableArray, NSMutableSet, NSMutableDictionary, DBRestClient, Cache, NSObject, DBAnalyticsLogger, NSArray, NSSet;

@interface FileOperationsManager : NSObject{

	NSString* _userId;
	DBFileCache* _fileCache;
	NSMutableArray* _foldersBeingCreated;
	NSMutableSet* _filesBeingDeleted;
	NSMutableSet* _filesBeingMoved;
	NSMutableDictionary* _fileMoveRequests;
	NSMutableSet* _metadataLoads;
	DBRestClient* _restClient;
	Cache* _cache;
	NSObject* _processMetadataQueue;
	BOOL _unlinked;
	DBAnalyticsLogger* _analyticsLogger;
	BOOL _firstSyncReported;
	NSString* _firstViewedPath;
	double _firstSyncStart;

}

@property (nonatomic,readonly) NSArray * foldersBeingCreated;              //@synthesize foldersBeingCreated=_foldersBeingCreated - In the implementation block
@property (nonatomic,readonly) NSSet * filesBeingDeleted;                  //@synthesize filesBeingDeleted=_filesBeingDeleted - In the implementation block
@property (nonatomic,readonly) NSSet * filesBeingMoved;                    //@synthesize filesBeingMoved=_filesBeingMoved - In the implementation block
@property (nonatomic,readonly) NSMutableSet * metadataLoads;               //@synthesize metadataLoads=_metadataLoads - In the implementation block
@property (nonatomic,readonly) DBRestClient * restClient;                  //@synthesize restClient=_restClient - In the implementation block
@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
+(id)db_errorStringForCreateFolderError:(id)arg1 ;
+(id)db_errorStringForDeleteError:(id)arg1 ;
+(id)db_errorStringForBatchDeleteError:(id)arg1 ;
+(id)db_errorStringForBatchMoveError:(id)arg1 ;
+(unsigned long long)db_failureReasonFromError:(id)arg1 ;
+(void)initialize;
-(DBRestClient *)restClient;
-(void)restClient:(id)arg1 loadedMetadata:(id)arg2 ;
-(void)restClient:(id)arg1 metadataUnchangedAtPath:(id)arg2 ;
-(void)restClient:(id)arg1 loadMetadataFailedWithError:(id)arg2 ;
-(void)restClient:(id)arg1 createdFolder:(id)arg2 ;
-(void)restClient:(id)arg1 createFolderFailedWithError:(id)arg2 ;
-(void)restClient:(id)arg1 deletedPath:(id)arg2 ;
-(void)restClient:(id)arg1 deletePathFailedWithError:(id)arg2 ;
-(void)createFolder:(id)arg1 ;
-(void)prepareToUnlink:(id)arg1 completion:(/*^block*/id)arg2 ;
-(void)db_loadedFileNotification:(id)arg1 ;
-(id)metadataKeyForPath:(id)arg1 ;
-(id)cachedMetadataForPath:(id)arg1 ;
-(void)db_loadMetadataAtPath:(id)arg1 withHash:(id)arg2 ;
-(id)db_directoryEntryContentsOfMetadata:(id)arg1 ;
-(id)metadataForPath:(id)arg1 ;
-(void)processMetadata:(id)arg1 ;
-(void)db_metadataRequestFinished:(id)arg1 ;
-(void)db_getCachedEntry:(id*)arg1 contents:(id*)arg2 forPath:(id)arg3 ;
-(id)db_directoryEntryForMetadata:(id)arg1 ;
-(BOOL)isLoadingMetadata:(id)arg1 ;
-(void)storeMetadata:(id)arg1 ;
-(id)initWithUserId:(id)arg1 usingFileCache:(id)arg2 ;
-(void)batchDeleteFiles:(id)arg1 ;
-(void)batchMoveFiles:(id)arg1 to:(id)arg2 ;
-(BOOL)isMetadataCachedForPath:(id)arg1 ;
-(void)invalidateCachedMetadataForPath:(id)arg1 ;
-(id)cachedContentsOfPath:(id)arg1 ;
-(id)contentsOfPath:(id)arg1 ;
-(BOOL)isCreatingFolder;
-(void)restClient:(id)arg1 batchDeletedPaths:(id)arg2 ;
-(void)restClient:(id)arg1 batchDeletePathsFailedWithError:(id)arg2 ;
-(void)restClient:(id)arg1 batchMovedPaths:(id)arg2 ;
-(void)restClient:(id)arg1 batchMovePathsFailedWithError:(id)arg2 ;
-(NSArray *)foldersBeingCreated;
-(NSSet *)filesBeingDeleted;
-(NSSet *)filesBeingMoved;
-(NSMutableSet *)metadataLoads;
-(void)dealloc;
-(void)deleteFile:(id)arg1 ;
@end

