//
//  jwjFilesInGroup.h
//  FavBox
//
//  Created by Weijie JIN on 16/11/2014.
//  Copyright (c) 2014 Wesley6j. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface jwjFilesInGroup : NSObject
@property(nonatomic,strong) NSMutableDictionary * listOfItems;

- (id) initWithURL:(NSURL *)url;
- (BOOL) saveToURL:(NSURL *)url;

- (void) addPath:(NSString *)path forUser:(NSString *)user;
- (void) removePath:(NSString *)path forUser:(NSString *)user;
- (BOOL) existPath:(NSString *)path forUser:(NSString *)user;
@end
